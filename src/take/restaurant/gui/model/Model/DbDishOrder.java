/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.restaurant.rest.Model;


import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
public class DbDishOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    
 
    private Integer id;
   
    private Integer dishQuantity;

    private DbOrder orderID;
    
    private dbDish dish;

    public dbDish getDish() {
        return dish;
    }

    public void setDish(dbDish dish) {
        this.dish = dish;
    }

    public DbDishOrder() {
    }

    public DbDishOrder(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDishQuantity() {
        return dishQuantity;
    }

    public void setDishQuantity(Integer dishQuantity) {
        this.dishQuantity = dishQuantity;
    }
    @XmlTransient
    public DbOrder getOrderID() {
        return orderID;
    }

    public void setOrderID(DbOrder orderID) {
        this.orderID = orderID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DbDishOrder)) {
            return false;
        }
        DbDishOrder other = (DbDishOrder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.restaurant.controller.util.DbDishOrder[ id=" + id + " ]";
    }

}
