/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.model.Model.List;

import com.restaurant.rest.Model.dbDishIndegrient;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
public class dbDishIndegrientList {

    public dbDishIndegrientList() {
    }
    
   
    private List<dbDishIndegrient> dbDishIndegrients;

    public dbDishIndegrientList(List<dbDishIndegrient> dbDishIndegrients) {
        this.dbDishIndegrients= dbDishIndegrients;
    }
     @XmlElementWrapper
    @XmlElement(name="dbDishIndegrient")
    public List<dbDishIndegrient> getDbDishIndegrients() {
        return dbDishIndegrients;
    }

    public void setDbDishIndegrients(List<dbDishIndegrient> dbDishIndegrients) {
        this.dbDishIndegrients = dbDishIndegrients;
    }

}
