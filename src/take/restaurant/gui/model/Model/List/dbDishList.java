/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.model.Model.List;

import com.restaurant.rest.Model.dbDish;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
public class dbDishList {
    
    public dbDishList() {
    }
  @XmlElementWrapper
    @XmlElement(name="dbDish")
    public List<dbDish> getDbDishList() {
        return dbDishList;
    }

    public void setDbDishList(List<dbDish> dbDishList) {
        this.dbDishList = dbDishList;
    }

  
    private List<dbDish> dbDishList;
}
