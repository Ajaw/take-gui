/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.model.Model.List;

import com.restaurant.rest.Model.DbDishOrder;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
public class DbDishOrderList {

    public DbDishOrderList() {
    }

    public DbDishOrderList(List<DbDishOrder> dishOrders) {
    
        this.dishOrders = dishOrders;
    }
    @XmlElementWrapper
    @XmlElement(name="DbDishOrder")
    public List<DbDishOrder> getDishOrders() {
    
        return dishOrders;
    }

    public void setDishOrders(List<DbDishOrder> dishOrders) {
        this.dishOrders = dishOrders;
    }
  
    private List<DbDishOrder> dishOrders;
}
