/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.model.Model.List;

import com.restaurant.rest.Model.DbIndegrientMagazine;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DbIndegrientMagazineList {
    @XmlElementWrapper
    @XmlElement(name="DbIndegrientMagazine")
    private List<DbIndegrientMagazine> indegrientMagazines = new ArrayList<>();

   
    public List<DbIndegrientMagazine> getIndegrientMagazines() {
        return indegrientMagazines;
    }

    public void setIndegrientMagazines(List<DbIndegrientMagazine> indegrientMagazines) {
        this.indegrientMagazines = indegrientMagazines;
    }


    
}
