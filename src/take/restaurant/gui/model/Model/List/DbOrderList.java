/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.model.Model.List;

import com.restaurant.rest.Model.DbOrder;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
public class DbOrderList {
    
      public DbOrderList(List<DbOrder> orders) {
        this.orders = orders;
    }

    public DbOrderList() {
    }
 @XmlElementWrapper
    @XmlElement(name="DbOrder")
    public List<DbOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<DbOrder> orders) {
        this.orders = orders;
    }
   
    private List<DbOrder> orders;

}
