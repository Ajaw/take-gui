/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.model.Model.List;

import com.restaurant.rest.Model.dbClient;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class dbClientList {   
    
   
    public List<dbClient> getClientList() {
        return clientList;
    }

    public void setClientList(List<dbClient> clientList) {
        this.clientList = clientList;
    }
   @XmlElementWrapper
    @XmlElement(name="dbClient")
    private List<dbClient> clientList;
}
