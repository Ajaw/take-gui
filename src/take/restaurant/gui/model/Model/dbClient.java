package com.restaurant.rest.Model;


import java.io.Serializable;
import java.util.Collection;

import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by Dominik on 02.04.2017.
 */
@XmlRootElement
public class dbClient implements Serializable{

    public dbClient() {
    }

    public dbClient(Integer id) {
        this.id = id;
    }

    private static final long serialVersionUID = 1L;  

    private Integer id;
 
    private String email;

    private String lastName;

    private String name;

    private String phone;
  
    private Collection<DbOrder> dbOrderCollection;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public Collection<DbOrder> getDbOrderCollection() {
        return dbOrderCollection;
    }

    public void setDbOrderCollection(Collection<DbOrder> dbOrderCollection) {
        this.dbOrderCollection = dbOrderCollection;
    }

 

}
