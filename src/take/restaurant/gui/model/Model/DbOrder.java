/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.restaurant.rest.Model;


import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
public class DbOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
   
    private Date orderDate;
 
    private dbClient clientId;  
    
    private Collection<DbDishOrder> dbDishOrderCollection;

    public DbOrder() {
    }

    private double orderPrice;

    public double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(double orderPrice) {
        this.orderPrice = orderPrice;
    }
    
    public DbOrder(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public dbClient getClientId() {
        return clientId;
    }

    public void setClientId(dbClient clientId) {
        this.clientId = clientId;
    }

    @XmlTransient
    public Collection<DbDishOrder> getDbDishOrderCollection() {
        return dbDishOrderCollection;
    }

    public void setDbDishOrderCollection(Collection<DbDishOrder> dbDishOrderCollection) {
        this.dbDishOrderCollection = dbDishOrderCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DbOrder)) {
            return false;
        }
        DbOrder other = (DbOrder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.restaurant.controller.util.DbOrder[ id=" + id + " ]";
    }

}
