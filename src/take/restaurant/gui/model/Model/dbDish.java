/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.restaurant.rest.Model;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
public class dbDish implements Serializable {

    private static final long serialVersionUID = 1L;
  
    private Integer id;
 
    private String dishName;
    
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
 
    private Collection<dbDishIndegrient> dbDishIndegrientCollection;

    public dbDish() {
    }

    public dbDish(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    @XmlTransient
    public Collection<dbDishIndegrient> getDbDishIndegrientCollection() {
        return dbDishIndegrientCollection;
    }

    public void setDbDishIndegrientCollection(Collection<dbDishIndegrient> dbDishIndegrientCollection) {
        this.dbDishIndegrientCollection = dbDishIndegrientCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof dbDish)) {
            return false;
        }
        dbDish other = (dbDish) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.restaurant.controller.util.dbDish[ id=" + id + " ]";
    }

}
