/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.restaurant.rest.Model;


import java.io.Serializable;
import java.util.Collection;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dominik
* @version 1.0
 */
@XmlRootElement
public class DbIndegrientMagazine implements Serializable {

    private static final long serialVersionUID = 1L;    
  
    private Integer id;

    private String messureType;
  
    private String name;

    private Double quantity;

    private Collection<dbDishIndegrient> dbDishIndegrientCollection;

    public DbIndegrientMagazine() {
    }

    public DbIndegrientMagazine(Integer id) {
        this.id = id;
    }
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessureType() {
        return messureType;
    }

    public void setMessureType(String messureType) {
        this.messureType = messureType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @XmlTransient
    public Collection<dbDishIndegrient> getDbDishIndegrientCollection() {
        return dbDishIndegrientCollection;
    }

    public void setDbDishIndegrientCollection(Collection<dbDishIndegrient> dbDishIndegrientCollection) {
        this.dbDishIndegrientCollection = dbDishIndegrientCollection;
    }

}
