/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package take.restaurant.gui;

import com.restaurant.rest.Model.DbIndegrientMagazine;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import take.restaurant.gui.DAO.ClientDAO;
import take.restaurant.gui.DAO.DishDAO;
import take.restaurant.gui.DAO.IndigrientMagazine;
import take.restaurant.gui.DAO.OrderDAO;
import take.restaurant.gui.DAO.remote.ClientDAORemote;
import take.restaurant.gui.DAO.remote.DishDAORemote;
import take.restaurant.gui.DAO.remote.IndegrientMagazineRemote;
import take.restaurant.gui.DAO.remote.OrderDAORemote;
import take.restaurant.gui.TabelModels.ClientTableModel;
import take.restaurant.gui.TabelModels.DishTableModel;
import take.restaurant.gui.TabelModels.IndegrientMagazineTableModel;
import take.restaurant.gui.TabelModels.OrderTableModel;

/**
 *
 * @author Dominik
 */
public class MainWindow extends javax.swing.JFrame {

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        initComponents();
        initDish();
        initClients();    
        initInidgrientMagazine();
        initOrders();
    }
    
    public void initOrders(){
         OrderTableModel orderTableModel = new OrderTableModel();
        OrderDAO dishDAO = new OrderDAORemote();
        orderTableModel.setDishDAO(dishDAO);
        orderTableModel.init();
        this.orderTableModel = orderTableModel;
        JTable orderTable = new JTable(orderTableModel);
        MainWindow mainWindow = this;
        this.orderTablePanel.setLayout(new BorderLayout());
        this.orderTablePanel.add(new JScrollPane(orderTable));
        this.editOrder.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {
                     if (orderTable.getSelectedRow()>=0) {
                          AddOrder addOrder = new AddOrder();
                        addOrder.setVisible(true);
                        addOrder.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        addOrder.setWindow(mainWindow);
                        addOrder.launchUpdateMode(orderTableModel.getObjectAtRow(orderTable.getSelectedRow()));
                }
             }
         });
       
    }
    
    public void initDish(){
        DishTableModel dishTableModel = new DishTableModel();
        DishDAO dishDAO = new DishDAORemote();
        dishTableModel.setDishDAO(dishDAO);
        dishTableModel.init();
        JTable currentTable = new JTable(dishTableModel);
        this.dishTablePanel.setLayout(new BorderLayout());
        this.dishTablePanel.add(new JScrollPane(currentTable));
           addDishButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               dishTableModel.addNew();
            }
        });
       this.editIndegrients.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentTable.getSelectedRow()>=0) {
                        AddIndegrientToDish addIndegrientToDish = new AddIndegrientToDish();
                addIndegrientToDish.setVisible(true);            
                addIndegrientToDish.setDishIndegrients(dishTableModel.getObjectAtRow(currentTable.getSelectedRow()));
                    addIndegrientToDish.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                }
             
            }
        });
                
    }
    
    public void initClients(){
        ClientTableModel clientTableModel = new ClientTableModel();
        ClientDAO clientDAO = new ClientDAORemote();
        clientTableModel.setMagazineDAO(clientDAO);
        clientTableModel.init();
        this.clientTablePanel.setLayout(new BorderLayout());
        this.clientTablePanel.add(new JScrollPane(new JTable(clientTableModel)));
        addClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               clientTableModel.addNew();
            }
        });
                
    }

    
    public void initInidgrientMagazine(){
 
      
        IndegrientMagazineTableModel tableModel = new IndegrientMagazineTableModel();
        IndigrientMagazine magazineDAO = new IndegrientMagazineRemote();
        tableModel.setMagazineDAO(magazineDAO);
        tableModel.init();
        this.tableMagazine = new JTable(tableModel);
        this.tablePanel.setLayout(new BorderLayout());
        
        this.tablePanel.setVisible(true);
       JScrollPane spTable = new JScrollPane(tableMagazine);
        this.tablePanel.add(spTable,BorderLayout.CENTER);    
        addMagazineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               tableModel.addNew();
            }
        });
//        this.updateMagazineButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                DbIndegrientMagazine indegrientMagazine = tableModel.getByIndex(tableMagazine.convertColumnIndexToModel(tableMagazine.getSelectedRow()));
//                tableModel.update(indegrientMagazine);
//            }
//        });
        this.tablePanel.revalidate();
        this.tablePanel.repaint();
               this.pack();
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        addMagazineButton = new javax.swing.JButton();
        tablePanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        orderTablePanel = new javax.swing.JPanel();
        chooseClientButton = new javax.swing.JButton();
        editOrder = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        dishTablePanel = new javax.swing.JPanel();
        addDishButton = new javax.swing.JButton();
        editIndegrients = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        clientTablePanel = new javax.swing.JPanel();
        addClientButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Take Restauracja GUI");

        addMagazineButton.setText("Dodaj Składnik");
        addMagazineButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addMagazineButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 635, Short.MAX_VALUE)
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addMagazineButton)
                .addGap(15, 15, 15)
                .addComponent(tablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(addMagazineButton)
                        .addContainerGap(296, Short.MAX_VALUE))))
        );

        jTabbedPane1.addTab("Magazyn", jPanel1);

        javax.swing.GroupLayout orderTablePanelLayout = new javax.swing.GroupLayout(orderTablePanel);
        orderTablePanel.setLayout(orderTablePanelLayout);
        orderTablePanelLayout.setHorizontalGroup(
            orderTablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 592, Short.MAX_VALUE)
        );
        orderTablePanelLayout.setVerticalGroup(
            orderTablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        chooseClientButton.setText("Dodaj zamówienie");
        chooseClientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseClientButtonActionPerformed(evt);
            }
        });

        editOrder.setText("Edytuj Zamówienie");
        editOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editOrderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chooseClientButton)
                    .addComponent(editOrder))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addComponent(orderTablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(orderTablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chooseClientButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(editOrder)
                .addContainerGap(262, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Zamówienia", jPanel2);

        javax.swing.GroupLayout dishTablePanelLayout = new javax.swing.GroupLayout(dishTablePanel);
        dishTablePanel.setLayout(dishTablePanelLayout);
        dishTablePanelLayout.setHorizontalGroup(
            dishTablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 616, Short.MAX_VALUE)
        );
        dishTablePanelLayout.setVerticalGroup(
            dishTablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        addDishButton.setText("Dodaj danie");

        editIndegrients.setText("Edytuj Składniki");
        editIndegrients.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editIndegrientsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(addDishButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editIndegrients, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(dishTablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dishTablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addDishButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(editIndegrients)
                .addContainerGap(262, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Dania", jPanel3);

        javax.swing.GroupLayout clientTablePanelLayout = new javax.swing.GroupLayout(clientTablePanel);
        clientTablePanel.setLayout(clientTablePanelLayout);
        clientTablePanelLayout.setHorizontalGroup(
            clientTablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 593, Short.MAX_VALUE)
        );
        clientTablePanelLayout.setVerticalGroup(
            clientTablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 330, Short.MAX_VALUE)
        );

        addClientButton.setText("Dodaj Kilenta");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(addClientButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addComponent(clientTablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(clientTablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addClientButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Klienci", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

   
    private void addMagazineButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addMagazineButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addMagazineButtonActionPerformed

    private void chooseClientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseClientButtonActionPerformed
        AddOrder addOrder = new AddOrder();
        addOrder.setVisible(true);
        addOrder.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addOrder.setWindow(this);
    }//GEN-LAST:event_chooseClientButtonActionPerformed

    private void editIndegrientsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editIndegrientsActionPerformed
      
    }//GEN-LAST:event_editIndegrientsActionPerformed

    private void editOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editOrderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editOrderActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addClientButton;
    private javax.swing.JButton addDishButton;
    private javax.swing.JButton addMagazineButton;
    private javax.swing.JButton chooseClientButton;
    private javax.swing.JPanel clientTablePanel;
    private javax.swing.JPanel dishTablePanel;
    private javax.swing.JButton editIndegrients;
    private javax.swing.JButton editOrder;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel orderTablePanel;
    private javax.swing.JPanel tablePanel;
    // End of variables declaration//GEN-END:variables
    private JTable tableMagazine;
    private IndegrientMagazineTableModel customTableModel;
    private OrderTableModel orderTableModel;

    public OrderTableModel getOrderTableModel() {
        return orderTableModel;
    }
    
    public void setCustomTableModel(IndegrientMagazineTableModel customTableModel) {
        this.customTableModel = customTableModel;
    }
    
    
}
