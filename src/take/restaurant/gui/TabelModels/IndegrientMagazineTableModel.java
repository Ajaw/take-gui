/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.TabelModels;

import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.DbIndegrientMagazine;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import take.restaurant.gui.DAO.IndigrientMagazine;
import take.restaurant.gui.DAO.DishIndegrientDAO;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class IndegrientMagazineTableModel extends AbstractTableModel {

   
	   IndigrientMagazine indegrientDAO;
	public void setMagazineDAO(IndigrientMagazine carDAO) {	this.indegrientDAO = carDAO; }

	List<DbIndegrientMagazine> magazineList = new ArrayList<DbIndegrientMagazine>();

	
	public void init() {
		magazineList = indegrientDAO.get();
                System.err.println(magazineList.size());
	}

	
	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public int getRowCount() {
		return magazineList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0: return magazineList.get(rowIndex).getId();
		case 1: return magazineList.get(rowIndex).getName();
		case 2: return magazineList.get(rowIndex).getMessureType();
		case 3: return magazineList.get(rowIndex).getQuantity();	
		}
		return null;
	}
        
        
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
		case 0: return "id";
		case 1: return "nazwa";
		case 2: return "jednostka";
		case 3: return "ilość";		
		}
		return "";
	}
        
         public DbIndegrientMagazine getObjectAtRow(int row){
            DbIndegrientMagazine indegrient = magazineList.get(row);
            return indegrient;
        }
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	@Override
	public void setValueAt(Object o,int rowIndex, int columnIndex) {
		try{
		DbIndegrientMagazine indegrientMagazine = magazineList.get(rowIndex);
		String txt = (String)o;
		switch(columnIndex) {
		case 0: indegrientMagazine.setId(Integer.parseInt(txt));break;
		case 1: indegrientMagazine.setName(txt);break;
		case 2: indegrientMagazine.setMessureType(txt);break;
		case 3: indegrientMagazine.setQuantity(Double.parseDouble(txt));break;		
		}
		indegrientDAO.update(indegrientMagazine);
		}catch(Exception e) {
		
			JOptionPane.showMessageDialog(null, "Bledna Wartosc!","Blad",JOptionPane.ERROR_MESSAGE);
		}
	}
        
        public void update(DbIndegrientMagazine indegrientMagazine){
            try {
                     indegrientDAO.update(indegrientMagazine);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Update error","Blad",JOptionPane.ERROR_MESSAGE);
            }
       
        }
	
        
        public DbIndegrientMagazine getByIndex(int index){
            return magazineList.get(index);
        }
	
	public void add(DbIndegrientMagazine indegrientMagazine) {
                System.err.println("adding indegrient to db");
		indegrientDAO.create(indegrientMagazine);                  
	
		magazineList = indegrientDAO.get();
                 
	
		fireTableDataChanged();
	}
	
	
	public void addNew() {
		DbIndegrientMagazine indegrientMagazine = new DbIndegrientMagazine();
                indegrientMagazine.setMessureType("a");
                indegrientMagazine.setName("a");
                indegrientMagazine.setQuantity(1.0);
		add(indegrientMagazine);
	}

	public void del(int rowIndex) {
		DbIndegrientMagazine indegrientMagazine = magazineList.get(rowIndex);
		indegrientDAO.delete(indegrientMagazine.getId());
		//od�wie� list� samochod�w
		magazineList = indegrientDAO.get();
		//powiadom tabel� JTable, �e trzeba od�wie�y� widok
		fireTableDataChanged();
	}
    
  

}
