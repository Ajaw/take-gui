/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.TabelModels;

import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Model.dbDish;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import take.restaurant.gui.DAO.ClientDAO;
import take.restaurant.gui.DAO.DishDAO;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class DishTableModel extends AbstractTableModel{

	   DishDAO dishDAO;
	public void setDishDAO(DishDAO dishDAO) {this.dishDAO = dishDAO; }

	
	List<dbDish> dishes = new ArrayList<dbDish>();

	
	public void init() {
		dishes = dishDAO.get();
	}

	
	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public int getRowCount() {
		return dishes.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0: return dishes.get(rowIndex).getId();
		case 1: return dishes.get(rowIndex).getDishName();
                case 2: return dishes.get(rowIndex).getPrice();                
		}
		return null;
	}
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
		case 0: return "id";
		case 1: return "nazwa dania";	
                case 2: return "cena";	
		}
		return "";
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
        public dbDish getObjectAtRow(int row){
            dbDish dish = dishes.get(row);
            return dish;
        }
        
	@Override
	public void setValueAt(Object o,int rowIndex, int columnIndex) {
		try{
		dbDish dish = dishes.get(rowIndex);
		String txt = (String)o;
		switch(columnIndex) {
		case 0: dish.setId(Integer.parseInt(txt));break;
		case 1: dish.setDishName(txt);break;
            	case 2:
                    double d = dish.getPrice();
                    try{
                        d = Integer.parseInt(txt);
                        dish.setPrice(d);
                        d = Double.parseDouble(txt);  
                          dish.setPrice(d);
                    }
                    catch (Exception ex){
                                             
                    }                    
                    break;
		
		}
		dishDAO.update(dish);
		}catch(Exception e) {
			
			JOptionPane.showMessageDialog(null, "Bledna wartosc!","Blad",JOptionPane.ERROR_MESSAGE);
		}
	}

    
	
	public void add(dbDish dish) {
		dishDAO.create(dish);
		
		dishes = dishDAO.get();

		fireTableDataChanged();
	}
	
	
	public void addNew() {
		dbDish dish = new dbDish();
                dish.setDishName("a");               
		add(dish);
	}
	
	public void del(int rowIndex) {
		dbDish dish = dishes.get(rowIndex);
		dishDAO.delete(dish.getId());
		
		dishes = dishDAO.get();
	
		fireTableDataChanged();
	}
}
