/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.TabelModels;

import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Model.dbDishIndegrient;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import take.restaurant.gui.DAO.ClientDAO;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class DishOrderTableModel extends AbstractTableModel{

	ClientDAO clientDAO;
	public void setMagazineDAO(ClientDAO clientDAO) {this.clientDAO = clientDAO; }


	List<DbDishOrder> dishOrders = new ArrayList<DbDishOrder>();

	
	public void init(List<DbDishOrder> dI) {
		this.dishOrders= dI;
                fireTableDataChanged();
	}

	
	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public int getRowCount() {
		return dishOrders.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0: return dishOrders.get(rowIndex).getId();
		case 1: return dishOrders.get(rowIndex).getDishQuantity();
		case 2: return dishOrders.get(rowIndex).getDish().getDishName();
		}
		return null;
	}
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
		case 0: return "id";
		case 1: return "ilosć";
		case 2: return "nazwa";			
		}
		return "";
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	@Override
	public void setValueAt(Object o,int rowIndex, int columnIndex) {
		try{
		DbDishOrder order = dishOrders.get(rowIndex);
		String txt = (String)o;
		switch(columnIndex) {

		case 1: 
                    try {
                        int value = Integer.parseInt(txt);
                        order.setDishQuantity(value); 
                    } catch (Exception e) {
                    }                   
                break;

		}
		
		}catch(Exception e) {
		
			JOptionPane.showMessageDialog(null, "Bledna warosc!","Blad",JOptionPane.ERROR_MESSAGE);
		}
	}
        
        public DbDishOrder getObjectAtRow(int row){
            DbDishOrder client = dishOrders.get(row);
            return client;
        }
	
    
}
