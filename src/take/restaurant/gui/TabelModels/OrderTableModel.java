/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.TabelModels;

import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Model.dbDish;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import take.restaurant.gui.DAO.DishDAO;
import take.restaurant.gui.DAO.OrderDAO;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class OrderTableModel extends AbstractTableModel{

	   OrderDAO orderDAO;
	public void setDishDAO(OrderDAO dishDAO) {this.orderDAO = dishDAO; }

	
	List<DbOrder> orders = new ArrayList<DbOrder>();

	
	public void init() {
		orders = orderDAO.get();
	}

	
	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public int getRowCount() {
		return orders.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0: return orders.get(rowIndex).getId();
		case 1: return orders.get(rowIndex).getOrderDate();
                case 2: return orders.get(rowIndex).getOrderPrice();
                case 3: 
                    if (orders.get(rowIndex).getClientId()!=null) {
                         dbClient client = orders.get(rowIndex).getClientId();
                         return client.getName() + " " + client.getLastName();
                    }  
                    return "";
                       
		}
		return null;
	}
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
		case 0: return "id";
		case 1: return "nazwa dania";
                case 2: return "wartosc zamowienia";	
                case 3: return "imie klienta";	
		}
		return "";
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
        
        public DbOrder getObjectAtRow(int row){
            DbOrder order = orders.get(row);
            return order;
        }
        
        public void updateTable(){
            this.init();
        }
	
	@Override
	public void setValueAt(Object o,int rowIndex, int columnIndex) {
		try{
		DbOrder dish = orders.get(rowIndex);
		String txt = (String)o;
		switch(columnIndex) {
		case 0: dish.setId(Integer.parseInt(txt));break;
		case 1: dish.setOrderDate(new Date(txt));break;
		
		}
		orderDAO.update(dish);
		}catch(Exception e) {
			
			JOptionPane.showMessageDialog(null, "Bledna wartosc!","Blad",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	public void add(DbOrder order) {
		orderDAO.create(order);
	
		orders = orderDAO.get();
	
		fireTableDataChanged();
	}
	
	
	public void addNew() {
		DbOrder order = new DbOrder();
                order.setOrderDate(new Date());               
		add(order);
	}
	
	public void del(int rowIndex) {
		DbOrder order = orders.get(rowIndex);
		orderDAO.delete(order.getId());
	
		orders = orderDAO.get();
		
		fireTableDataChanged();
	}
}
