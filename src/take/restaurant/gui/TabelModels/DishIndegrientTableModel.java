/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.TabelModels;

import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Model.dbDishIndegrient;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import take.restaurant.gui.DAO.ClientDAO;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class DishIndegrientTableModel extends AbstractTableModel{
	
	ClientDAO clientDAO;
        
	public void setMagazineDAO(ClientDAO clientDAO) {this.clientDAO = clientDAO; }

	
	List<dbDishIndegrient> dishIndegrients = new ArrayList<dbDishIndegrient>();

	
	public void init(List<dbDishIndegrient> dI) {
		this.dishIndegrients= dI;
                fireTableDataChanged();
	}

	
	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public int getRowCount() {
		return dishIndegrients.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0: return dishIndegrients.get(rowIndex).getId();
		case 1: return dishIndegrients.get(rowIndex).getMessureType();
		case 2: return dishIndegrients.get(rowIndex).getQuantity();
                case 3: return dishIndegrients.get(rowIndex).getIndegrietnMagazineId().getName();
		}
		return null;
	}
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
		case 0: return "id";
		case 1: return "typ miary";
		case 2: return "ilość";	
                case 3: return "nazwa składniku";	
		}
		return "";
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	@Override
	public void setValueAt(Object o,int rowIndex, int columnIndex) {
		try{
		dbDishIndegrient client = dishIndegrients.get(rowIndex);
		String txt = (String)o;
		switch(columnIndex) {

		case 2:
                    try {
                        double d=  Double.parseDouble(txt);
                        client.setQuantity(d);
                    } catch (Exception e) {
                        System.err.println("parsing error");
                    }
                    
                    break;

		}
		
		}catch(Exception e) {
			 
			JOptionPane.showMessageDialog(null, "Bledna wartosc!","Blad",JOptionPane.ERROR_MESSAGE);
		}
	}
        
        public dbDishIndegrient getObjectAtRow(int row){
            dbDishIndegrient dishIndgirent = dishIndegrients.get(row);
            return dishIndgirent;
        }

	public void add(dbClient client) {

		fireTableDataChanged();
	}
	


	public void del(int rowIndex) {

		fireTableDataChanged();
	}
}
