/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.TabelModels;

import javax.swing.table.AbstractTableModel;
import com.restaurant.rest.Model.DbIndegrientMagazine;
import com.restaurant.rest.Model.dbClient;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import take.restaurant.gui.DAO.ClientDAO;
import take.restaurant.gui.DAO.IndigrientMagazine;
import take.restaurant.gui.DAO.DishIndegrientDAO;
/**
 *
 * @author Dominik
* @version 1.0
 */
public class ClientTableModel extends AbstractTableModel{
  	
	ClientDAO clientDAO;
	public void setMagazineDAO(ClientDAO clientDAO) {this.clientDAO = clientDAO; }


	List<dbClient> clients = new ArrayList<dbClient>();



	public void init() {
		clients = clientDAO.get();
	}

	
	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public int getRowCount() {
		return clients.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0: return clients.get(rowIndex).getId();
		case 1: return clients.get(rowIndex).getName();
		case 2: return clients.get(rowIndex).getLastName();
		case 3: return clients.get(rowIndex).getEmail();	
                case 4: return clients.get(rowIndex).getPhone();	
		}
		return null;
	}
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
		case 0: return "id";
		case 1: return "Imie";
		case 2: return "Nazwisko";
		case 3: return "Email";	
                case 4: return "Telefon";	
		}
		return "";
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	@Override
	public void setValueAt(Object o,int rowIndex, int columnIndex) {
		try{
		dbClient client = clients.get(rowIndex);
		String txt = (String)o;
		switch(columnIndex) {
		case 0: client.setId(Integer.parseInt(txt));break;
		case 1: client.setName(txt);break;
		case 2: client.setLastName(txt);break;
		case 3: client.setEmail(txt);break;		
                case 4: client.setPhone(txt);break;
		}
		clientDAO.update(client);
		}catch(Exception e) {
			// w przypadku b��du zmiana odrzucona i komunikat o b��dzie 
			JOptionPane.showMessageDialog(null, "Bledna wartosc!","Blad",JOptionPane.ERROR_MESSAGE);
		}
	}
        
        public dbClient getObjectAtRow(int row){
            dbClient client = clients.get(row);
            return client;
        }
	

	public void add(dbClient client) {
		clientDAO.create(client);
		
		clients = clientDAO.get();
		
		fireTableDataChanged();
	}
	
	
	public void addNew() {
		dbClient client = new dbClient();
                client.setLastName("a");
                client.setName("a");
                client.setEmail("a");
                client.setPhone("a");
		add(client);
	}
	
	public void del(int rowIndex) {
		dbClient client = clients.get(rowIndex);
		clientDAO.delete(client.getId());
		
		clients = clientDAO.get();
		
		fireTableDataChanged();
	}
    
}
