/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.DAO;

import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Model.dbDishIndegrient;
import java.util.List;
import take.restaurant.gui.model.Model.List.dbClientList;

/**
 *
 * @author Dominik
* @version 1.0
 */
public interface ClientDAO {
    
	public abstract void create(dbClient car);

	public abstract dbClient find(int idc);

	public abstract List<dbClient> get();

	public abstract void update(dbClient car);

	public abstract void delete(int idc);

}
