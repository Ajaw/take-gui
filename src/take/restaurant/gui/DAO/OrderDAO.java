/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package take.restaurant.gui.DAO;

import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Model.dbClient;
import java.util.List;

/**
 *
 * @author Dominik
 */
public interface OrderDAO {
    
    public abstract DbOrder create(DbOrder order);

	public abstract DbOrder find(int idc);

	public abstract List<DbOrder> get();

	public abstract void update(DbOrder order);

	public abstract void delete(int idc);
    
}
