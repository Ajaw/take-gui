/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.DAO;

import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Model.dbDishIndegrient;
import java.util.List;
import take.restaurant.gui.model.Model.List.dbDishList;

/**
 *
 * @author Dominik
* @version 1.0
 */
public interface DishDAO {
    	public abstract void create(dbDish dish);

	public abstract dbDish find(int idc);

	public abstract List<dbDish> get();

	public abstract void update(dbDish dish);

	public abstract void delete(int idc);

}
