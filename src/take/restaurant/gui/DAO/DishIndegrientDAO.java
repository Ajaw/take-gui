/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package take.restaurant.gui.DAO;

import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Model.dbDishIndegrient;
import java.util.List;
import take.restaurant.gui.model.Model.List.dbDishIndegrientList;

/**
 *
 * @author Dominik
 */
public interface DishIndegrientDAO {
    
    
   	public abstract void create(dbDishIndegrient car);

	public abstract dbDishIndegrient find(int idc);

	public abstract List<dbDishIndegrient> get();

	public abstract void update(dbDishIndegrient car);

	public abstract void delete(int idc);
        
        public abstract List<dbDishIndegrient> getIndegrientsForDish(dbDish dish);
        
        public abstract void bulkUpdate(dbDishIndegrientList dbDishIndegrientList1);
    
}
