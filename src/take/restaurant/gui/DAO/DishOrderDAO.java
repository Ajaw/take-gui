/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package take.restaurant.gui.DAO;

import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Model.dbDish;
import java.util.List;
import take.restaurant.gui.model.Model.List.DbDishOrderList;

/**
 *
 * @author Dominik
 */
public interface DishOrderDAO {
    
    	public abstract void create(DbDishOrder dish);

	public abstract DbDishOrder find(int idc);

	public abstract List<DbDishOrder> get();
        
        public abstract void bulkUpdate(DbDishOrderList dbDishOrderList);

	public abstract void update(DbDishOrder dish);

	public abstract void delete(int idc);
        
        public abstract List<DbDishOrder> getDishesForOrder(DbOrder order);

}
