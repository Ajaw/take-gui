/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.DAO.remote;

import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Model.dbDishIndegrient;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import take.restaurant.gui.DAO.DishDAO;
import take.restaurant.gui.Utils.HttpHelper;
import take.restaurant.gui.model.Model.List.dbClientList;
import take.restaurant.gui.model.Model.List.dbDishList;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class DishDAORemote implements DishDAO {

     String url = "http://localhost.:8080/take-rest/rest/dish";
    
    @Override
    public void create(dbDish dish) {
         StringWriter writer = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(dbDish.class);
            Marshaller marshaller = context.createMarshaller();
      
        marshaller.marshal(dish, writer);
       
            
            
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(null, "error", "InfoBox: " , JOptionPane.INFORMATION_MESSAGE);
        }
         String test = writer.toString();
        HttpHelper.doPost(url+"/create", writer.toString(), "application/xml");
    }

    @Override
    public dbDish find(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<dbDish> get() {
        String txt = HttpHelper.doGet(url+"/get");
        JAXBContext context;
       try {
            context = JAXBContext.newInstance(dbDishList.class);
       
        
            Unmarshaller unmarshaller = context.createUnmarshaller();     

           if (txt==null) {
               return new LinkedList<>();
           }
        StreamSource reader = new StreamSource(new StringReader(txt));
           dbDishList dishList = (dbDishList) unmarshaller.unmarshal(reader);
           if (dishList.getDbDishList()!=null) {                     
                return dishList.getDbDishList();
           }
          } catch (JAXBException ex) {
            Logger.getLogger(IndegrientMagazineRemote.class.getName()).log(Level.SEVERE, null, ex);
        }
       return new LinkedList<>();
    }    

    @Override
    public void update(dbDish dish) {
          StringWriter writer = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(dbDish.class);
            Marshaller marshaller = context.createMarshaller();        
        marshaller.marshal(dish, writer);
        String test = writer.toString();            
            
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(null, "error", "InfoBox: " , JOptionPane.INFORMATION_MESSAGE);
        }
        
        HttpHelper.doPost(url+"/update", writer.toString(), "application/xml");
    }

    @Override
    public void delete(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

}
