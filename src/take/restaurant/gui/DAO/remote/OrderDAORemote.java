/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.DAO.remote;

import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Model.dbClient;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import take.restaurant.gui.DAO.OrderDAO;
import take.restaurant.gui.Utils.HttpHelper;
import take.restaurant.gui.model.Model.List.DbOrderList;
import take.restaurant.gui.model.Model.List.dbClientList;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class OrderDAORemote implements OrderDAO{

    String url = "http://localhost.:8080/take-rest/rest/order";
    
    @Override
    public DbOrder create(DbOrder order) {
              StringWriter writer = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(DbOrder.class);
            Marshaller marshaller = context.createMarshaller();
      
        marshaller.marshal(order, writer);
       
            
            
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(null, "error", "InfoBox: " , JOptionPane.INFORMATION_MESSAGE);
        }
         String test = writer.toString();
        String response = HttpHelper.doPost(url+"/create", writer.toString(), "application/xml");
          try {
           JAXBContext context = JAXBContext.newInstance(DbOrder.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
      
      
            DbOrder order1 = (DbOrder) unmarshaller.unmarshal(new StringReader(response));
            return order1;
        } catch (JAXBException ex) {
            Logger.getLogger(OrderDAORemote.class.getName()).log(Level.SEVERE, null, ex);
        }
          return null;
    }

    @Override
    public DbOrder find(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DbOrder> get() {
         String txt = HttpHelper.doGet(url+"/get");
        JAXBContext context;
       try {
            context = JAXBContext.newInstance(DbOrderList.class);
       
        
            Unmarshaller unmarshaller = context.createUnmarshaller();     

           if (txt==null) {
               return new LinkedList<>();
           }
        StreamSource reader = new StreamSource(new StringReader(txt));
           DbOrderList magazineList = (DbOrderList) unmarshaller.unmarshal(reader);
           
        return magazineList.getOrders();
          } catch (JAXBException ex) {
            Logger.getLogger(IndegrientMagazineRemote.class.getName()).log(Level.SEVERE, null, ex);
        }
       return new LinkedList<>();
    }

    @Override
    public void update(DbOrder order) {
              StringWriter writer = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(DbOrder.class);
            Marshaller marshaller = context.createMarshaller();        
        marshaller.marshal(order, writer);
        String test = writer.toString();            
            
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(null, "error", "InfoBox: " , JOptionPane.INFORMATION_MESSAGE);
        }
        
        HttpHelper.doPost(url+"/update", writer.toString(), "application/xml");
    }

    @Override
    public void delete(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
