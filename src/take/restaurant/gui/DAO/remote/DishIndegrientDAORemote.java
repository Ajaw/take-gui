/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.DAO.remote;

import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Model.dbDishIndegrient;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import take.restaurant.gui.Utils.HttpHelper;
import take.restaurant.gui.model.Model.List.DbDishOrderList;
import take.restaurant.gui.model.Model.List.dbDishIndegrientList;
import take.restaurant.gui.model.Model.List.dbDishList;
import take.restaurant.gui.DAO.DishIndegrientDAO;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class DishIndegrientDAORemote implements DishIndegrientDAO {

    String url = "http://localhost.:8080/take-rest/rest/dishindegrient";
    
    @Override
    public void create(dbDishIndegrient car) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public dbDishIndegrient find(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<dbDishIndegrient> get() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(dbDishIndegrient car) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    @Override
     public void bulkUpdate(dbDishIndegrientList dbDishIndegrientList1) {
         StringWriter writer = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(dbDishIndegrientList.class);
            Marshaller marshaller = context.createMarshaller();
      
        marshaller.marshal(dbDishIndegrientList1, writer);
       
            
            
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(null, "error", "InfoBox: " , JOptionPane.INFORMATION_MESSAGE);
        }
         String test = writer.toString();
        HttpHelper.doPost(url+"/bulkupdate", writer.toString(), "application/xml");
    }
     
     @Override
     public List<dbDishIndegrient> getIndegrientsForDish(dbDish dish){
            
            StringWriter writer= new StringWriter();
        JAXBContext context;
       try {
            context = JAXBContext.newInstance(dbDish.class);
            Marshaller marshaller = context.createMarshaller();
             marshaller.marshal(dish,writer);
            String data = writer.toString();
            String response = HttpHelper.doPost(url+"/getindegrientsfordish",data,"application/xml");
            context = JAXBContext.newInstance(dbDishIndegrientList.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();     
            
           if (response==null) {
               return new LinkedList<>();
           }
        StreamSource reader = new StreamSource(new StringReader(response));
           dbDishIndegrientList dishList = (dbDishIndegrientList) unmarshaller.unmarshal(reader);
           if (dishList.getDbDishIndegrients()!=null) {                     
                return dishList.getDbDishIndegrients();
           }
          } catch (JAXBException ex) {
            Logger.getLogger(IndegrientMagazineRemote.class.getName()).log(Level.SEVERE, null, ex);
        }
       return new LinkedList<>();
     }
    
}
