/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.DAO.remote;

import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Model.dbDishIndegrient;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import take.restaurant.gui.Utils.HttpHelper;
import take.restaurant.gui.model.Model.List.DbDishOrderList;
import take.restaurant.gui.DAO.DishOrderDAO;
import take.restaurant.gui.model.Model.List.dbDishIndegrientList;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class DishOrderDAORemote implements DishOrderDAO{
    
  String url = "http://localhost.:8080/take-rest/rest/dishorder";
  
    @Override
    public void create(DbDishOrder dish) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DbDishOrder find(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DbDishOrder> get() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void bulkUpdate(DbDishOrderList dbDishOrderList) {
         StringWriter writer = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(DbDishOrderList.class);
            Marshaller marshaller = context.createMarshaller();
      
        marshaller.marshal(dbDishOrderList, writer);
       
            
            
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(null, "error", "InfoBox: " , JOptionPane.INFORMATION_MESSAGE);
        }
         String test = writer.toString();
        HttpHelper.doPost(url+"/bulkupdate", writer.toString(), "application/xml");
    }

    @Override
    public void update(DbDishOrder dish) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
     @Override
     public List<DbDishOrder> getDishesForOrder(DbOrder order){
            
            StringWriter writer= new StringWriter();
        JAXBContext context;
       try {
            context = JAXBContext.newInstance(DbOrder.class);
            Marshaller marshaller = context.createMarshaller();
             marshaller.marshal(order,writer);
            String data = writer.toString();
            String response = HttpHelper.doPost(url+"/getdishesfororder",data,"application/xml");
            context = JAXBContext.newInstance(DbDishOrderList.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();     
            
           if (response==null) {
               return new LinkedList<>();
           }
        StreamSource reader = new StreamSource(new StringReader(response));
           DbDishOrderList dishList = (DbDishOrderList) unmarshaller.unmarshal(reader);
           if (dishList.getDishOrders()!=null) {                     
                return dishList.getDishOrders();
           }
          } catch (JAXBException ex) {
            Logger.getLogger(IndegrientMagazineRemote.class.getName()).log(Level.SEVERE, null, ex);
        }
       return new LinkedList<>();
     }

}
