/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.DAO.remote;

import com.restaurant.rest.Model.DbIndegrientMagazine;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import take.restaurant.gui.DAO.IndigrientMagazine;
import take.restaurant.gui.Utils.HttpHelper;
import take.restaurant.gui.model.Model.List.DbIndegrientMagazineList;

/**
 *
 * @author Dominik
* @version 1.0
 */
public class IndegrientMagazineRemote implements IndigrientMagazine {

    String url = "http://localhost.:8080/take-rest/rest/indegrient_mag";
    
    @Override
    public void create(DbIndegrientMagazine indegrientMagazine) {
        StringWriter writer = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(DbIndegrientMagazine.class);
            Marshaller marshaller = context.createMarshaller();
           // marshaller.setProperty("eclipselink.media-type", "application/json");
            // Set it to true if you need to include the JSON root element in the JSON output

       // marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

         

        // Set it to true if you need the JSON output to formatted

       // marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(indegrientMagazine, writer);
        String test = writer.toString();
            
            
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(null, "error", "InfoBox: " , JOptionPane.INFORMATION_MESSAGE);
        }
        
        HttpHelper.doPost(url+"/create", writer.toString(), "application/xml");
    }

    @Override
    public DbIndegrientMagazine find(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DbIndegrientMagazine> get() {
        String txt = HttpHelper.doGet(url+"/get");
        JAXBContext context;
       try {
            context = JAXBContext.newInstance(DbIndegrientMagazineList.class);
       
        
            Unmarshaller unmarshaller = context.createUnmarshaller();
      
           // unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
       // unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, false        );
         // unmarshaller.setProperty(UnmarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);

       
        StreamSource reader = new StreamSource(new StringReader(txt));
        DbIndegrientMagazineList magazineList = (DbIndegrientMagazineList) unmarshaller.unmarshal(reader);
        return magazineList.getIndegrientMagazines();
          } catch (JAXBException ex) {
            Logger.getLogger(IndegrientMagazineRemote.class.getName()).log(Level.SEVERE, null, ex);
        }
       return new LinkedList<>();
    }

    @Override
    public void update(DbIndegrientMagazine indegrientMagazine) {
       StringWriter writer = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(DbIndegrientMagazine.class);
            Marshaller marshaller = context.createMarshaller();        
        marshaller.marshal(indegrientMagazine, writer);
        String test = writer.toString();            
            
        } catch (JAXBException ex) {
            JOptionPane.showMessageDialog(null, "error", "InfoBox: " , JOptionPane.INFORMATION_MESSAGE);
        }
        
        HttpHelper.doPost(url+"/update", writer.toString(), "application/xml");
    }

    @Override
    public void delete(int idc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
