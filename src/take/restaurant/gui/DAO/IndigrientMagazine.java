/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package take.restaurant.gui.DAO;

import com.restaurant.rest.Model.DbIndegrientMagazine;
import java.util.List;

/**
 *
 * @author Dominik
* @version 1.0
 */
public interface IndigrientMagazine {
public abstract void create(DbIndegrientMagazine car);

	public abstract DbIndegrientMagazine find(int idc);

	public abstract List<DbIndegrientMagazine> get();

	public abstract void update(DbIndegrientMagazine car);

	public abstract void delete(int idc);
}
